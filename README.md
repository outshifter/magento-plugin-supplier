# Mage2 Module Outshifter Connector

``outshifter/connector``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Outshifter Connector

## Installation

### Via Composer

```
composer require outshifter/connector:1.0.0
php bin/magento module:enable Outshifter_Outshifter
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento cache:flush
```

## To Remove

```
php bin/magento module:disable Outshifter_Outshifter --clear-static-content
php bin/magento module:uninstall Outshifter_Outshifter --remove-data --clear-static-content
```

## Configuration

Go to Stores > Configuration > Outshifter > Api key, and save your outshifter api key

Go to System > Integrations, and authorize the Outshifter integration

## Attributes

 - Product - exported_outshifter (exported_outshifter)

