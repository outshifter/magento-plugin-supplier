<?php

namespace Outshifter\Outshifter\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Data extends AbstractHelper
{

  /**
   * @param Context $context
   */
  public function __construct(
    Context $context
  ) {
    parent::__construct($context);
  }

  /*
     * @return string
     */
  public function getApiUrl($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
  {
    return $this->scopeConfig->getValue(
      'outshifter_section/outshifter_config/apiurl',
      $scope
    );
  }

  /*
     * @return string
     */
  public function getApiKey($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
  {
    return $this->scopeConfig->getValue(
      'outshifter_section/outshifter_config/apikey',
      $scope
    );
  }
}
