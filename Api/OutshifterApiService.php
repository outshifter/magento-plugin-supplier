<?php

namespace Outshifter\Outshifter\Api;

interface OutshifterApiService
{

  /**
   * POST order in magento store
   * 
   * @api
   * @param Outshifter\Outshifter\Api\Data\CustomerDtoInterface $customer The customer data
   * @param Outshifter\Outshifter\Api\Data\ItemsDtoInterface[] $items The items data
   * @return string The orderId
   */
  public function saveOrder($customer, $items);

  /**
   * DELETE order in magento store
   * 
   * @api
   * @param int $id The order id
   * @return int
   */
  public function cancelOrder($id);

  /**
   * DELETE item order in magento store
   * 
   * @api
   * @param int $orderId The order id
   * @param int $productId The product id
   * @return string
   */
  public function cancelItemOrder($orderId, $productId);

  /**
   * POST ship order in magento store
   * 
   * @api
   * @param int $id The order id
   * @param string $trackingNumber The tracking number
   * @param string $slug The carrier name
   * @param string $url The tracking url
   * @return string
   */
  public function shipOrder($id, $trackingNumber, $slug, $url);

  /**
   * GET currency magento store
   * @return string
   */
  public function getCurrency();
}
