<?php

namespace Outshifter\Outshifter\Model;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\ShipmentItemCreationInterfaceFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Sales\Model\Convert\Order;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Outshifter\Outshifter\Api\OutshifterApiService;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteManagement;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Outshifter\Outshifter\Helper\Utils;
use Outshifter\Outshifter\Logger\Logger;

class OutshifterApiServiceImpl implements OutshifterApiService
{

  /**
   * @var StoreManagerInterface
   */
  protected $storeManager;

  /**
   * @var CustomerFactory
   */
  protected $customerFactory;

  /**
   * @var CustomerRepositoryInterface
   */
  protected $customerRepository;

  /**
   * @var OrderManagementInterface
   */
  protected $orderManagement;

  /**
   * @var TrackFactory
   */
  protected $trackingFactory;

  /**
   * @var ShipmentItemCreationInterfaceFactory
   */
  protected $shipmentItemInterface;

  /**
   * @var OrderRepositoryInterface
   */
  protected $orderRepository;

  /**
   * @var Order
   */
  protected $orderConverter;

  /**
   * @var QuoteFactory
   */
  protected $quoteFactory;

  /**
   * @var QuoteManagement
   */
  protected $quoteManagement;

  /**
   * @var ProductRepositoryInterface
   */
  protected $productRepository;

  /**
   * @var Utils
   */
  protected $utils;

  /**
   * @var Logger
   */
  protected $_logger;

  public function __construct(
    StoreManagerInterface $storeManager,
    CustomerFactory $customerFactory,
    CustomerRepositoryInterface $customerRepository,
    OrderManagementInterface $orderManagement,
    TrackFactory $trackingFactory,
    ShipmentItemCreationInterfaceFactory $shipmentItemInterface,
    OrderRepositoryInterface $orderRepository,
    Order $orderConverter,
    QuoteFactory $quoteFactory,
    QuoteManagement $quoteManagement,
    ProductRepositoryInterface $productRepository,
    Utils $utils,
    Logger $logger
  ) {
    $this->storeManager = $storeManager;
    $this->customerFactory = $customerFactory;
    $this->customerRepository = $customerRepository;
    $this->orderManagement = $orderManagement;
    $this->trackingFactory = $trackingFactory;
    $this->shipmentItemInterface = $shipmentItemInterface;
    $this->orderRepository = $orderRepository;
    $this->orderConverter = $orderConverter;
    $this->quoteFactory = $quoteFactory;
    $this->quoteManagement = $quoteManagement;
    $this->productRepository = $productRepository;
    $this->utils = $utils;
    $this->_logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function saveOrder($customerDto, $itemsDto)
  {
    $store = $this->storeManager->getStore();
    $websiteId = $this->storeManager->getStore()->getWebsiteId();
    $quote = $this->quoteFactory->create();
    $quote->setStore($store);
    $billingAddress = [
      'firstname'    => $customerDto->getBillingName(),
      'lastname'     => '-',
      'street' => $customerDto->getBillingStreet(),
      'city' => $customerDto->getBillingCity(),
      'region' => $customerDto->getBillingRegion(),
      'country_id' => $customerDto->getBillingCountry(),
      'postcode' => $customerDto->getBillingZip(),
      'telephone' => $customerDto->getBillingPhone(),
      'fax' => $customerDto->getBillingPhone(),
      'save_in_address_book' => 0
    ];
    $shippingAddress = [
      'firstname'    => $customerDto->getShippingName(),
      'lastname'     => '-',
      'street' => $customerDto->getShippingStreet(),
      'city' => $customerDto->getShippingCity(),
      'region' => $customerDto->getShippingRegion(),
      'country_id' => $customerDto->getShippingCountry(),
      'postcode' => $customerDto->getShippingZip(),
      'telephone' => $customerDto->getShippingPhone(),
      'fax' => $customerDto->getShippingPhone(),
      'save_in_address_book' => 0
    ];
    $this->_logger->info('[OutshifterApi.saveOrder] Creating order to customer ' . $customerDto->getEmail());

    $customer = $this->customerFactory->create();
    $customer->setWebsiteId($websiteId);
    $customer->loadByEmail($customerDto->getEmail());
    if (!$customer->getEntityId()) {
      //If not avilable then create this customer 
      $customer->setWebsiteId($websiteId)
        ->setStore($store)
        ->setFirstname($customerDto->getBillingName())
        ->setLastname("-")
        ->setEmail($customerDto->getEmail())
        ->setPassword($customerDto->getEmail());
      $customer->save();
    }

    $this->_logger->info('[OutshifterApi.saveOrder] Customer created');

    $customer = $this->customerRepository->getById($customer->getEntityId());
    $quote->setCurrency();
    $quote->assignCustomer($customer);
    foreach ($itemsDto as $item) {
      $product = $this->productRepository->getById($item->getProductId());
      $this->_logger->info('[OutshifterApi.saveOrder] adding product ' . $product->getId() . ' qty ' . $item->getQuantity());
      $quote->addProduct($product, intval($item->getQuantity()));
    }
    $this->_logger->info('[OutshifterApi.saveOrder] Products added');

    $quote->getBillingAddress()->addData($billingAddress);
    $quote->getShippingAddress()->addData($shippingAddress);

    $shippingAddress = $quote->getShippingAddress();
    $shippingAddress->setCollectShippingRates(true)
      ->collectShippingRates()
      ->setShippingMethod('freeshipping_freeshipping');
    $this->_logger->info('[OutshifterApi.saveOrder] Shipping method setted');

    $quote->setPaymentMethod('checkmo'); //payment method
    $quote->setInventoryProcessed(false); //not effetc inventory
    $quote->save();

    $this->_logger->info('[OutshifterApi.saveOrder] Payment method setted');

    // Set Sales Order Payment
    $quote->getPayment()->importData(['method' => 'checkmo']);

    // Collect Totals & Save Quote
    $quote->collectTotals()->save();

    $this->_logger->info('[OutshifterApi.saveOrder] Quote prepared');

    // Create Order From Quote
    $order = $this->quoteManagement->submit($quote);

    $orderId = $order->getEntityId();

    $this->_logger->info('[OutshifterApi.saveOrder] Order ' . $orderId . ' created');

    return $orderId;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelOrder($id)
  {
    $this->_logger->info('[OutshifterApi.cancelOrder] with id ' . $id);
    $this->orderManagement->cancel($id);
    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelItemOrder($orderId, $productId)
  {
    $this->_logger->info('[OutshifterApi.cancelItemOrder] orderId ' . $orderId . ', productId ' . $productId);
    $order = $this->orderRepository->get($orderId);
    $itemPrice = 0;
    foreach ($order->getAllVisibleItems() as $item) {
      if ($productId == $item->getProductId()) {
        $this->_logger->info('[OutshifterApi.cancelItemOrder] deleting item ' . $item->getId() . ' with price ' . $item->getData('row_total_incl_tax'));
        $itemPrice = $item->getData('row_total_incl_tax');
        $item->isDeleted(true);
        $this->_logger->info('[OutshifterApi.cancelItemOrder] itemId ' . $item->getId() . ' deleted');
      }
    }
    $subtotal = $order->getSubtotal() - $itemPrice;
    $grandTotal = $order->getGrandTotal() - $itemPrice;
    $baseGrandTotal = $order->getBaseGrandTotal() - $itemPrice;
    $this->_logger->info('[OutshifterApi.cancelItemOrder] new subtotal ' . $subtotal . ', new grandTotal ' . $grandTotal . ', new baseGrandTotal ' . $baseGrandTotal);
    $order->setSubtotal($subtotal);
    $order->setGrandTotal($grandTotal);
    $order->setBaseGrandTotal($baseGrandTotal);
    $order->save();
    return $orderId;
  }

  /**
   * {@inheritdoc}
   */
  public function shipOrder($id, $trackingNumber, $slug, $url)
  {
    $this->_logger->info('[OutshifterApi.shipOrder] with id ' . $id . ', trackingNumber ' . $trackingNumber);
    $order = $this->orderRepository->get($id);
    if ($order->canShip()) {
      $shipment = $this->orderConverter->toShipment($order);
      foreach ($order->getAllItems() as $orderItem) {
        $qtyShipped = $orderItem->getQtyToShip();
        $shippedItem = $this->orderConverter->itemToShipmentItem($orderItem)->setQty($qtyShipped);
        $shipment->addItem($shippedItem);
      }
      if ($trackingNumber) {
        $track = $this->trackingFactory->create();
        $track->setTrackNumber($trackingNumber);
        $track->setCarrierCode('custom');
        $track->setTitle($slug);
        $track->setUrl($url);
        $shipment->addTrack($track);
      }
      $shipment->addComment('Shipped in outshifter');

      $shipment->register();
      $shipment->getOrder()->setIsInProcess(true);
      $shipment->save();
      $shipment->getOrder()->save();
      $this->_logger->info('[OutshifterApi.shipOrder] order ' . $id . ' shipped with id ' . $shipment->getIncrementId());
      return $shipment->getIncrementId();
    }
    return __('The order %1 not can be processed', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrency()
  {
    try {
      $response = $this->utils->getCurrencyStore();
    } catch (\Exception $e) {
      $response = $e->getMessage();
    }

    return $response;
  }
}
