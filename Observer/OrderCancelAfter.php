<?php

namespace Outshifter\Outshifter\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Outshifter\Outshifter\Logger\Logger;
use Outshifter\Outshifter\Helper\OutshifterService;

class OrderCancelAfter implements ObserverInterface
{

  /**
   * @var Logger
   */
  protected $_logger;

  /**
   * @var OutshifterService
   */
  protected $outshifterService;

  public function __construct(
    OutshifterService $outshifterService,
    Logger $logger
  ) {
    $this->outshifterService = $outshifterService;
    $this->_logger = $logger;
  }

  public function execute(Observer $observer)
  {
    $order = $observer->getEvent()->getOrder();
    $this->_logger->info('[OrderShipmentSaveAfter] order ' . $order->getId() . ' has been canceled.');
    $this->outshifterService->markOrderAsCancel($order);
  }
}
